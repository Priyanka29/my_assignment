from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie.authentication import Authentication
from .models import *


class CitizenResource(ModelResource):
	class Meta:
		queryset = Citizen.objects.all()
		resource_name = 'citizen'
		authorization = Authorization()
		authentication = Authentication()