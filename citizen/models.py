from django.db import models
from django.core.files import File

class Citizen(models.Model):
	CITIES = (
		('MUMBAI', 'Mumbai'),
		('KOLHAPUR', 'Kolhapur'),
		('THANE', 'Thane'),
		('PUNE', 'Pune')
	)
	name = models.CharField(max_length=300)
	date_of_birth = models.DateField()
	email = models.EmailField()
	moibile = models.IntegerField()
	address_line1 = models.CharField(max_length=200)
	address_line_2 = models.CharField(max_length=200)
	city = models.CharField(choices=CITIES,max_length=20)
	pincode = models.IntegerField()
	qualification = models.CharField(max_length=20)
	photo = models.ImageField(upload_to='photos')
	aadhar = models.FileField(upload_to='aadhar')
	pan = models.FileField(upload_to='pan')
	ration = models.FileField(upload_to='ration')
	driving_license	 = models.FileField(upload_to='driving_license')
	
	def __str__(self):
		return self.name
